export class Tarjeta {
    titulo: String;
    subtitulo: String;
    descripcion: String;
    constructor(t: String, s: String, d: String) {
        this.titulo = t;
        this.subtitulo = s;
        this.descripcion = d;
    }
}