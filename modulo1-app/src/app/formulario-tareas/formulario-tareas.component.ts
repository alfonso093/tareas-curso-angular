import { Component, OnInit, Input } from '@angular/core';
import { Tarjeta } from '../../models/Tarjeta.model';

@Component({
  selector: 'app-formulario-tareas',
  templateUrl: './formulario-tareas.component.html',
  styleUrls: ['./formulario-tareas.component.css']
})
export class FormularioTareasComponent implements OnInit {
  tareas: Tarjeta[];
  titulo1: String;
  constructor() {
    this.titulo1 = "Tareas pendientes"
    this.tareas = []
  }

  ngOnInit(): void {
  }

  guardar(t: String, s: String, d: String): boolean {
    this.tareas.push(new Tarjeta(t, s, d));
    return false
  }
}
