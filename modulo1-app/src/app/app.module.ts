import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListaTareasComponent } from './lista-tareas/lista-tareas.component';
import { FormularioTareasComponent } from './formulario-tareas/formulario-tareas.component';
import { CardTareaComponent } from './card-tarea/card-tarea.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaTareasComponent,
    FormularioTareasComponent,
    CardTareaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
