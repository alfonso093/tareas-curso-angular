import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {
  titulo:String;
  tareas:String[];
  constructor() { 
    this.titulo = "Lista de frutas";
    this.tareas = ['Manzana','Banana','Pera','Sandía','Tuna'];
  }

  ngOnInit(): void {
  }

}
