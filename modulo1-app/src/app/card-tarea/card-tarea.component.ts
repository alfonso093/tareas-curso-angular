import { Component, OnInit, HostBinding ,Input} from '@angular/core';
import { Tarjeta } from '../../models/Tarjeta.model';

@Component({
  selector: 'app-card-tarea',
  templateUrl: './card-tarea.component.html',
  styleUrls: ['./card-tarea.component.css']
})
export class CardTareaComponent implements OnInit {
  @Input() tarjeta:Tarjeta;
  @HostBinding('attr.class') cssClass = 'col-md-3';
  constructor() { }

  ngOnInit(): void {
  }

}
